# Ansible Collection - s3lph.dirvish

WIP

## Usage

1. Use the contents of `docs/` as a template for your playbook directory
1. Run `ansible-galaxy install -r requirements.yml`
1. Add your backup servers as well as backed-up clients to the inventory
  - Don't add the clients all at once; instead run the playbook after adding each client and initialize the client
1. Add the snapshot scripts required for your services to the `templates/` directory
1. Configure the variables in `group_vars/` and `host_vars/`
1. TBD ...